package ma.octo.assignement;

import ma.octo.assignement.entities.Compte;
import ma.octo.assignement.entities.Transaction;
import ma.octo.assignement.entities.Utilisateur;
import ma.octo.assignement.repositories.CompteRepository;
import ma.octo.assignement.repositories.TransactionRepository;
import ma.octo.assignement.repositories.UtilisateurRepository;
import ma.octo.assignement.services.CompteService;
import ma.octo.assignement.services.TransactionService;
import ma.octo.assignement.services.UtilisateurService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.math.BigDecimal;
import java.util.Date;

@SpringBootApplication
public class AssignementApplication implements CommandLineRunner {
	@Autowired
	private CompteService compteService;
	@Autowired
	private UtilisateurService utilisateurService;
	@Autowired
	private TransactionService transactionService;

	

	public static void main(String[] args) {
		SpringApplication.run(AssignementApplication.class, args);
	}

	@Override
	public void run(String... strings) throws Exception {
		
		

		Utilisateur utilisateur1 = new Utilisateur();
		utilisateur1.setUsername("user1");
		utilisateur1.setLastname("last1");
		utilisateur1.setFirstname("first1");
		utilisateur1.setPassword("password1");
		utilisateur1.setRole("User");
		utilisateur1.setGender("male");

		utilisateurService.sauvegarder(utilisateur1);


		Utilisateur utilisateur2 = new Utilisateur();
		utilisateur2.setUsername("user2");
		utilisateur2.setLastname("last2");
		utilisateur2.setFirstname("first2");
		utilisateur1.setPassword("password2");
		utilisateur1.setRole("User");
		utilisateur2.setGender("Female");
		utilisateurService.sauvegarder(utilisateur2);

		


		Compte compte1 = new Compte();
		compte1.setNrCompte("010000A000001000");
		compte1.setRib("RIB1");
		compte1.setSolde(BigDecimal.valueOf(200000L));
		compte1.setUtilisateur(utilisateur1);

		compteService.sauvegarder(compte1);

		Compte compte2 = new Compte();
		compte2.setNrCompte("010000B025001000");
		compte2.setRib("RIB2");
		compte2.setSolde(BigDecimal.valueOf(140000L));
		compte2.setUtilisateur(utilisateur2);

		compteService.sauvegarder(compte2);

		Transaction transaction = new Transaction();
		transaction.setMontantVirement(BigDecimal.TEN);
		transaction.setCompteBeneficiaire(compte2);
		transaction.setCompteEmetteur(compte1);
		transaction.setDateExecution(new Date());
		transaction.setMotifVirement("Assignment 2021");

		transactionService.sauvegarder(transaction);
	}
}
