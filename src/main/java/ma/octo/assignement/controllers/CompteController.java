package ma.octo.assignement.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ma.octo.assignement.dto.TransactionDto;
import ma.octo.assignement.entities.Compte;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.services.CompteService;
@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class CompteController {
	
	 @Autowired
	    private CompteService compteService;
	 @GetMapping("/comptes")
	    List<Compte> loadAllCompte() {
	    return   compteService.find_comptes_list();        
	    }
	 @PostMapping("/virements")// enlever les verbes utiliser les noms sans majuscules
	    @ResponseStatus(HttpStatus.CREATED)
	    public void createvirement(@RequestBody TransactionDto transactionDto)
	            throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {
	        compteService.appliquer_transaction(transactionDto);

	        
	    }
	 @PostMapping("/versement")// enlever les verbes utiliser les noms sans majuscules
	    @ResponseStatus(HttpStatus.CREATED)
	    public void createversement(@RequestBody TransactionDto transactionDto)
	            throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {
	        compteService.appliquer_transaction(transactionDto);

	        
	    }
	 
}
