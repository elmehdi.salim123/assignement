package ma.octo.assignement.controllers;

import ma.octo.assignement.dto.TransactionDto;
import ma.octo.assignement.entities.Compte;
import ma.octo.assignement.entities.Transaction;
import ma.octo.assignement.entities.Utilisateur;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repositories.CompteRepository;
import ma.octo.assignement.repositories.TransactionRepository;
import ma.octo.assignement.repositories.UtilisateurRepository;
import ma.octo.assignement.services.AuditTransactionService;
import ma.octo.assignement.services.CompteService;
import ma.octo.assignement.services.TransactionService;
import ma.octo.assignement.services.UtilisateurService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;
@CrossOrigin(origins = "http://localhost:4200")
@RestController
class TransactionController {

   
    @Autowired
    private TransactionService transactionService;
    

    @GetMapping("/transaction")
    List<Transaction> loadAll() {
    	return transactionService.find_virement_list();
    }

   

   

   

   
}
