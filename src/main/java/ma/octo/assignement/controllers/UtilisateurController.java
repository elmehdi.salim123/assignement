package ma.octo.assignement.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import ma.octo.assignement.entities.Utilisateur;
import ma.octo.assignement.services.UtilisateurService;
@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class UtilisateurController {
	@Autowired
    private UtilisateurService utilisateurService;
	 @GetMapping("/utilisateurs")
	    List<Utilisateur> loadAllUtilisateur() {
	     
	    	return  utilisateurService.find_utilisateur_liste();
	    }
}
