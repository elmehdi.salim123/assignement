package ma.octo.assignement.mapper;

import ma.octo.assignement.dto.TransactionDto;
import ma.octo.assignement.entities.Compte;
import ma.octo.assignement.entities.Transaction;

public class VirementMapper {

   
  
    

    public static TransactionDto map_to_transaction(Transaction transaction) {
    	TransactionDto transactionDto = new TransactionDto();
    	transactionDto.setEmetteur(transaction.getCompteEmetteur().getNrCompte());
    	transactionDto.setDate(transaction.getDateExecution());
    	transactionDto.setMotif(transaction.getMotifVirement());

        return transactionDto;

    }
    
    public static Transaction map_to_transactionDto(TransactionDto transactionDto) {
    	Transaction transaction = new Transaction();
    	Compte emetteur = new Compte();
    	Compte beneficiaire = new Compte();
    	emetteur.setNrCompte(transactionDto.getEmetteur());
    	beneficiaire.setNrCompte(transactionDto.getNrCompteBeneficiaire());
    	transaction.setCompteBeneficiaire(beneficiaire);
    	transaction.setCompteEmetteur(null);
    	transaction.setDateExecution(transactionDto.getDate());
    	transaction.setMontantVirement(transactionDto.getMontantVirement());
    	transaction.setMotifVirement(transactionDto.getMotif());
    	return transaction;
    	
    }
}
//ajouter test
// ajouter l'inverse
// fait attention virement et virsement