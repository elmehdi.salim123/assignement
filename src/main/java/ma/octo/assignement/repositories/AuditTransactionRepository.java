package ma.octo.assignement.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import ma.octo.assignement.entities.AuditTransaction;

public interface AuditTransactionRepository extends JpaRepository<AuditTransaction, Long> {
}
