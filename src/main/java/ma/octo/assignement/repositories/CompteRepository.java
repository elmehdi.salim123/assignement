package ma.octo.assignement.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import ma.octo.assignement.entities.Compte;

public interface CompteRepository extends JpaRepository<Compte, Long> {
  Compte findByNrCompte(String nrCompte);
}
