package ma.octo.assignement.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import ma.octo.assignement.entities.Transaction;

public interface TransactionRepository extends JpaRepository<Transaction, Long> {
}
