package ma.octo.assignement.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import ma.octo.assignement.entities.Utilisateur;

public interface UtilisateurRepository extends JpaRepository<Utilisateur, Long> {

	Utilisateur findByUsername(String username);
}
