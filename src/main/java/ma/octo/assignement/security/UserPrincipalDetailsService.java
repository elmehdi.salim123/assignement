package ma.octo.assignement.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import ma.octo.assignement.entities.Utilisateur;
import ma.octo.assignement.repositories.UtilisateurRepository;

public class UserPrincipalDetailsService implements UserDetailsService{
	
	@Autowired
	UtilisateurRepository utilisateurRepository;
	
@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		Utilisateur utilisateur=utilisateurRepository.findByUsername(username);
		UserPrincipal userPrincipal= new UserPrincipal(utilisateur);
		return userPrincipal;
	}

}
