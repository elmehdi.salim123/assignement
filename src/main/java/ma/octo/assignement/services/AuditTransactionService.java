package ma.octo.assignement.services;

import ma.octo.assignement.domain.util.EventType;
import ma.octo.assignement.entities.AuditTransaction;
import ma.octo.assignement.repositories.AuditTransactionRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class AuditTransactionService {

    Logger LOGGER = LoggerFactory.getLogger(AuditTransactionService.class);

    @Autowired
    private AuditTransactionRepository auditTransactionRepository;

    public void audit(String message,String type) {

    

        AuditTransaction audit = new AuditTransaction();
        if(type.equals("versement")) {
        	LOGGER.info("Audit de l'événement {}", EventType.VERSEMENT);
            audit.setEventType(EventType.VERSEMENT);
        	
        }else {
        	LOGGER.info("Audit de l'événement {}", EventType.VIREMENT);
            audit.setEventType(EventType.VIREMENT);
        }
        audit.setMessage(message);
        auditTransactionRepository.save(audit);
    }


    
}
