package ma.octo.assignement.services;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ma.octo.assignement.dto.TransactionDto;
import ma.octo.assignement.entities.Compte;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repositories.CompteRepository;

@Service
public class CompteService {
	 public static final int MONTANT_MAXIMAL = 10000;
	 Logger logger = LoggerFactory.getLogger(CompteService.class);
@Autowired
CompteRepository compteRepository;

@Autowired
TransactionService transactionService;
@Autowired
AuditTransactionService auditService;

public List<Compte> find_comptes_list() {
	  List<Compte> all = compteRepository.findAll();

      if (CollectionUtils.isEmpty(all)) {
          return null;
      } else {
          return all;
      }
	
}

public void appliquer_transaction(TransactionDto transactionDto) throws SoldeDisponibleInsuffisantException, TransactionException, CompteNonExistantException {
	String type = new String("viresement");
	// beneficiaire sera dans les 2 cas
	Compte emetteur = new Compte();
	Compte beneficiaire = new Compte();
	if(transactionDto.getEmetteur().matches("^[a-zA-Z0-9]+$"))
		
	{
		emetteur = compteRepository.findByNrCompte(transactionDto.getEmetteur());
		 if (emetteur == null) {
		    	logger.error("Compte Non existant");
		        throw new CompteNonExistantException("Compte Non existant");
		    }
		 type = "virement";
	}
	else {
		emetteur.setNrCompte(transactionDto.getEmetteur());
	}
    beneficiaire = compteRepository.findByNrCompte(transactionDto.getNrCompteBeneficiaire());
	
	
    if(emetteur.getNbvirement() >= 10) {
    	logger.error("nb de virement");
    }
   
    

    if (beneficiaire == null) {
    	logger.error("Compte Non existant");
        throw new CompteNonExistantException("Compte Non existant");
    }

    if (transactionDto.getMontantVirement().equals(null)) {
        logger.error("Montant vide");
        throw new TransactionException("Montant vide");
    } else if (transactionDto.getMontantVirement().intValue() == 0) {
    	logger.error("Montant vide");
        throw new TransactionException("Montant vide");
    } else if (transactionDto.getMontantVirement().intValue() < 10) {
    	logger.error("Montant minimal de virement non atteint");
        throw new TransactionException("Montant minimal de virement non atteint");
    } else if (transactionDto.getMontantVirement().intValue() > MONTANT_MAXIMAL) {
    	logger.error("Montant maximal de virement dÃ©passÃ©");
        throw new TransactionException("Montant maximal de virement dÃ©passÃ©");
    }

    if (transactionDto.getMotif().length() <= 0) {
    	logger.error("Motif vide");
        throw new TransactionException("Motif vide");
    }

    

   
// possible seulement dans le cas de virement
   if(type.equals("virement")) {
	   if (emetteur.getSolde().doubleValue() - transactionDto.getMontantVirement().doubleValue() < 0) {
	    	logger.error("solde insiffusant");
	        throw new SoldeDisponibleInsuffisantException("solde insiffusant");
	    }
	   emetteur.setNbvirement(emetteur);
	   emetteur.setSolde(emetteur.getSolde().subtract(transactionDto.getMontantVirement()));
	    compteRepository.save(emetteur);
   }
  

    beneficiaire.setSolde(new BigDecimal(beneficiaire.getSolde().intValue() + transactionDto.getMontantVirement().intValue()));
    compteRepository.save(beneficiaire);
    transactionService.ajouter_virement(transactionDto.getDate(), emetteur, beneficiaire, transactionDto.getMontantVirement(),type);
    
	
} 
public void sauvegarder(Compte compte) {
	compteRepository.save(compte);
	
}
}
