package ma.octo.assignement.services;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import ma.octo.assignement.entities.Compte;
import ma.octo.assignement.entities.Transaction;
import ma.octo.assignement.repositories.CompteRepository;
import ma.octo.assignement.repositories.TransactionRepository;

@Service
public class TransactionService {
	@Autowired
	TransactionRepository transactionRepository;
	
	@Autowired
	AuditTransactionService auditService;

	public List<Transaction> find_virement_list() {
		List<Transaction> all = transactionRepository.findAll();

        if (CollectionUtils.isEmpty(all)) {
            return null;
        } else {
            return all;
        }
		
	} 
	
	public void ajouter_virement(Date date,Compte emetteur,Compte beneficiaire,BigDecimal montant,String type) {
		Transaction virement = new Transaction();
        virement.setDateExecution(date);
        virement.setCompteBeneficiaire(beneficiaire);
        virement.setCompteEmetteur(null);
        virement.setMontantVirement(montant);

        transactionRepository.save(virement);
        
        auditService.audit("Virement depuis " + emetteur.getNrCompte() + " vers " + beneficiaire.getNrCompte() + " d'un montant de " + montant.toString(),type);
	}
	
	public void sauvegarder(Transaction transaction) {
		transactionRepository.save(transaction);
		
	}

}
