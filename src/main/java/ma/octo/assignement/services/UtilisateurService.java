package ma.octo.assignement.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import ma.octo.assignement.entities.Utilisateur;
import ma.octo.assignement.repositories.UtilisateurRepository;

@Service
public class UtilisateurService {
	@Autowired
	UtilisateurRepository utilisateurRepository;

	public List<Utilisateur> find_utilisateur_liste() {
		   List<Utilisateur> all = utilisateurRepository.findAll();

	        if (CollectionUtils.isEmpty(all)) {
	            return null;
	        } else {
	            return all;
	        }
		
	}
	public void sauvegarder(Utilisateur utilisateur) {
		utilisateurRepository.save(utilisateur);
		
	}
	
}
