package ma.octo.assignement.services;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import ma.octo.assignement.dto.TransactionDto;
import ma.octo.assignement.entities.Compte;
import ma.octo.assignement.entities.Transaction;
import ma.octo.assignement.entities.Utilisateur;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;

@ContextConfiguration
@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
public class CompteServiceTest {
	@Autowired
	private CompteService compteService;
	@Autowired
	private TransactionService transactionService;

	@Test
	public void find_comptes_list_test(){
		Utilisateur utilisateur1 = new Utilisateur();
		utilisateur1.setUsername("user1");
		utilisateur1.setLastname("last1");
		utilisateur1.setFirstname("first1");
		utilisateur1.setPassword("password1");
		utilisateur1.setRole("User");
		utilisateur1.setGender("male");

		


		Utilisateur utilisateur2 = new Utilisateur();
		utilisateur2.setUsername("user2");
		utilisateur2.setLastname("last2");
		utilisateur2.setFirstname("first2");
		utilisateur1.setPassword("password2");
		utilisateur1.setRole("User");
		utilisateur2.setGender("Female");
		

		
		List<Compte> list = new ArrayList<Compte>();

		Compte compte1 = new Compte();
		compte1.setNrCompte("010000A000001000");
		compte1.setRib("RIB1");
		compte1.setSolde(BigDecimal.valueOf(200000L));
		compte1.setUtilisateur(utilisateur1);

		

		Compte compte2 = new Compte();
		compte2.setNrCompte("010000B025001000");
		compte2.setRib("RIB2");
		compte2.setSolde(BigDecimal.valueOf(140000L));
		compte2.setUtilisateur(utilisateur2);
		
		list.add(compte1);
		list.add(compte2);
		assertEquals(list, compteService.find_comptes_list());
		
		
	}
	
	@Test
	public void appliquer_transaction_test() throws Exception, TransactionException, CompteNonExistantException {
		// d'abord creere les utilisateur et comptes 
		// creer virement manuellement
		// apres creer virement avec la methode de service
		// assert equals
		
		Utilisateur utilisateur1 = new Utilisateur();
		utilisateur1.setUsername("user1");
		utilisateur1.setLastname("last1");
		utilisateur1.setFirstname("first1");
		utilisateur1.setPassword("password1");
		utilisateur1.setRole("User");
		utilisateur1.setGender("male");

		
		Utilisateur utilisateur2 = new Utilisateur();
		utilisateur2.setUsername("user2");
		utilisateur2.setLastname("last2");
		utilisateur2.setFirstname("first2");
		utilisateur1.setPassword("password2");
		utilisateur1.setRole("User");
		utilisateur2.setGender("Female");


		Compte compte1 = new Compte();
		compte1.setNrCompte("010000A000001000");
		compte1.setRib("RIB1");
		compte1.setSolde(BigDecimal.valueOf(200000L));
		compte1.setUtilisateur(utilisateur1);

		
		Compte compte2 = new Compte();
		compte2.setNrCompte("010000B025001000");
		compte2.setRib("RIB2");
		compte2.setSolde(BigDecimal.valueOf(140000L));
		compte2.setUtilisateur(utilisateur2);
		
		List<Transaction> list = new ArrayList<Transaction>();
		
		Transaction transaction = new Transaction();
		transaction.setMontantVirement(BigDecimal.TEN);
		transaction.setCompteBeneficiaire(compte2);
		transaction.setCompteEmetteur(compte1);
		transaction.setDateExecution(new Date());
		transaction.setMotifVirement("Assignment 2021");
		
		Transaction transaction2 = new Transaction();
		transaction2.setMontantVirement(BigDecimal.TEN);
		transaction2.setCompteBeneficiaire(compte2);
		transaction2.setCompteEmetteur(compte1);
		transaction2.setDateExecution(new Date());
		transaction2.setMotifVirement("Assignment 2021");
		
		list.add(transaction);
		list.add(transaction2);
		
		TransactionDto transactionDto = new TransactionDto();
		transactionDto.setDate(new Date());
		transactionDto.setEmetteur(compte1.getNrCompte());
		transactionDto.setMontantVirement(BigDecimal.TEN);
		transactionDto.setMotif("Assignment 2021");
		transactionDto.setNrCompteBeneficiaire(compte2.getNrCompte());
		
		
		
		compteService.appliquer_transaction(transactionDto);
		
		
		assertEquals(list,transactionService.find_virement_list());
		
		
		
		
	}
}
