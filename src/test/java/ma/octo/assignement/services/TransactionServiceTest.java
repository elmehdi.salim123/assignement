package ma.octo.assignement.services;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import ma.octo.assignement.entities.Compte;
import ma.octo.assignement.entities.Transaction;
import ma.octo.assignement.entities.Utilisateur;

@ContextConfiguration
@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
public class TransactionServiceTest {
	@Autowired
	private TransactionService transactionService;
	@Test
	public void find_virement_list_test() {
		
		Utilisateur utilisateur1 = new Utilisateur();
		utilisateur1.setUsername("user1");
		utilisateur1.setLastname("last1");
		utilisateur1.setFirstname("first1");
		utilisateur1.setPassword("password1");
		utilisateur1.setRole("User");
		utilisateur1.setGender("male");

		


		Utilisateur utilisateur2 = new Utilisateur();
		utilisateur2.setUsername("user2");
		utilisateur2.setLastname("last2");
		utilisateur2.setFirstname("first2");
		utilisateur1.setPassword("password2");
		utilisateur1.setRole("User");
		utilisateur2.setGender("Female");
		

		


		Compte compte1 = new Compte();
		compte1.setNrCompte("010000A000001000");
		compte1.setRib("RIB1");
		compte1.setSolde(BigDecimal.valueOf(200000L));
		compte1.setUtilisateur(utilisateur1);

		

		Compte compte2 = new Compte();
		compte2.setNrCompte("010000B025001000");
		compte2.setRib("RIB2");
		compte2.setSolde(BigDecimal.valueOf(140000L));
		compte2.setUtilisateur(utilisateur2);

		

		Transaction transaction = new Transaction();
		transaction.setMontantVirement(BigDecimal.TEN);
		transaction.setCompteBeneficiaire(compte2);
		transaction.setCompteEmetteur(compte1);
		transaction.setDateExecution(new Date());
		transaction.setMotifVirement("Assignment 2021");
		
		List<Transaction> list =new ArrayList<Transaction>();
		list.add(transaction);
		
		assertEquals(list, transactionService.find_virement_list());

		
	}
	
	

}
