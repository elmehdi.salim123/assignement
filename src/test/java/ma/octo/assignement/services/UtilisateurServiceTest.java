package ma.octo.assignement.services;

import static org.junit.Assert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import ma.octo.assignement.entities.Utilisateur;

@ContextConfiguration
@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
public class UtilisateurServiceTest {
	@Autowired
	private UtilisateurService utilisateurService;
	
	

	@Test
	public void find_utilisateur_liste_test() {
		List<Utilisateur> list = new ArrayList<Utilisateur>();
		Utilisateur utilisateur1 = new Utilisateur();
		utilisateur1.setUsername("user1");
		utilisateur1.setLastname("last1");
		utilisateur1.setFirstname("first1");
		utilisateur1.setPassword("password1");
		utilisateur1.setRole("User");
		utilisateur1.setGender("male");
		list.add(utilisateur1);


		Utilisateur utilisateur2 = new Utilisateur();
		utilisateur2.setUsername("user2");
		utilisateur2.setLastname("last2");
		utilisateur2.setFirstname("first2");
		utilisateur1.setPassword("password2");
		utilisateur1.setRole("User");
		utilisateur2.setGender("Female");
		list.add(utilisateur2);
		
	assertEquals(list, utilisateurService.find_utilisateur_liste());
		
		
		
	}
}
